package modellayer;


/**
 * Write a description of class DangerRoom here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class DangerRoom extends Room
{
    // instance variables - replace the example below with your own
    private int damage;
    /**
     * Constructor for objects of class DangerRoom
     */
    public DangerRoom(int floor, int yCoord, int xCoord, String description, int damage)
    {
        super(floor, yCoord, xCoord, description);
        this.damage = damage;
        // initialise instance variables
    }
    
    public int getDamage()
    {
        return damage;
    }
}

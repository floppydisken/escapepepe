package modellayer;

/**
 * Enumeration class CommandWord - write a description of the enum class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public enum CommandWord
{
    HELP, GO, INSPECT, LOOK, MAP, QUIT, UNKNOWN;
    
    public String toString()
    {
        switch(this)
        {
            
            case HELP:
                return "help";
            case GO:
                return "go";
            case INSPECT:
                return "inspect";
            case LOOK:
                return "look";
            case MAP:
                return "map";
            case QUIT:
                return "quit";
            case UNKNOWN:
                return "unknown";
            default:
                return null;
        }
    }
}
package modellayer;


/**
 * Enumeration class SoundType - write a description of the enum class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public enum SoundType
{
    AIRHORN, DOOROPEN, FOUNDITEM, NOFOUNDITEM, DUNDUN
}

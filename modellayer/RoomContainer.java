package modellayer;
import java.util.ArrayList;
/**
 * Write a description of class RoomContainer here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RoomContainer
{
    // instance variables - replace the example below with your own
    private static RoomContainer instance;
    private ArrayList<Room> rooms;
    /**
     * Constructor for objects of class RoomContainer
     */
    private RoomContainer()
    {
        // initialise instance variables
        rooms = new ArrayList<>();
    }
    public static RoomContainer getInstance()
    {
        if (instance == null)
            instance = new RoomContainer();
        return instance;
    }
    
    public void addRoom(Room room)
    {
        rooms.add(room);
    }
    
    public Room findRoom(int floor, int yCoord, int xCoord)
    {
        boolean found = false;
        int index = 0;
        while(!found && index < rooms.size())
        {
            Room currentRoom = rooms.get(index);
            if(currentRoom.getFloor() == floor
                && currentRoom.getYCoord() == yCoord
                && currentRoom.getXCoord() == xCoord)
            {
                found = true;
            } 
            else 
            {
                index++;
            }
        }
        
        if (found)
        {
            return rooms.get(index);
        }
        else
        {
            return null;
        }
    }
    
    public ArrayList<Room> findRooms()
    {
        ArrayList<Room> result = new ArrayList<>();
        for (Room room : rooms)
            result.add(room);
            
        return result;
    }
    
    public int getLowestYCoord(int floor)
    {
        int lowestYCoord = Integer.MAX_VALUE;
        
        for (Room room : rooms)
            if (room.getFloor() == floor && room.getYCoord() < lowestYCoord)
                lowestYCoord = room.getYCoord();
        
        return lowestYCoord;
    }
    
    public int getLowestXCoord(int floor)
    {
        int lowestXCoord = Integer.MAX_VALUE;
        
        for (Room room : rooms)
            if (room.getFloor() == floor && room.getXCoord() < lowestXCoord)
                lowestXCoord = room.getXCoord();
        
        return lowestXCoord;
    }
    
    public int getHighestYCoord(int floor)
    {
        int highestYCoord = 0;
        
        for (Room room : rooms)
            if (room.getFloor() == floor && room.getYCoord() > highestYCoord)
                highestYCoord = room.getYCoord();
        
        return highestYCoord;
    }
    
    public int getHighestXCoord(int floor)
    {
        int highestXCoord = 0;
        
        for (Room room : rooms)
            if (room.getFloor() == floor && room.getXCoord() > highestXCoord)
                highestXCoord = room.getXCoord();
        
        return highestXCoord;
    }
}
package modellayer;

/**
 * Write a description of class Command here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Command
{
    // instance variables - replace the example below with your own
    private CommandWord firstWord;
    private String secondWord;
    private Direction direction;
    /**
     * Constructor for objects of class Command
     */
    public Command()
    {
        
    }
    
    public static Command parseCommand(String line)
    {
        String[] words = line.split("\\s+");
        Command result = new Command();
        
        if (words.length >= 1)
        {
            try
            {
                result.setFirstWord(CommandWord.valueOf(words[0].toUpperCase()));
                if(words.length >= 2)
                {
                    result.setSecondWord(words[1]);
                    if(result.getFirstWord() == CommandWord.GO)
                        result.setDirection(result.getSecondWord());
                    else
                        result.setSecondWord(line.substring(words[0].length() + 1));
                }
            }
            catch (IllegalArgumentException e)
            {
                result.setFirstWord(CommandWord.UNKNOWN);
            }
        }
        else
            result.setFirstWord(CommandWord.UNKNOWN);
        
        return result;
    }
    
    private void setDirection(String input)
    {
        try
        {
            direction = Direction.valueOf(input.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            direction = null;
        }
    }
    
    public Direction getDirection()
    {
        return direction;
    }
    
    public void setFirstWord(CommandWord firstWord)
    {
        this.firstWord = firstWord;
    }
    
    public CommandWord getFirstWord()
    {
        return firstWord;
    }
    
    public void setSecondWord(String secondWord)
    {
        this.secondWord = secondWord;
    }
    
    public String getSecondWord()
    {
        return secondWord;
    }
}

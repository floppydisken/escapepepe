package modellayer;

/**
 * Enumeration class Direction
 *
 * @author Stiig
 * @version 04/04
 */
public enum Direction
{
    NORTH, EAST, SOUTH, WEST, UP, DOWN;
    
    public Direction getOpposite()
    {
        Direction opposite = null;
        
        switch (this)
        {
            case NORTH:
                opposite = Direction.SOUTH;
                break;
            case EAST:
                opposite = Direction.WEST;
                break;
            case SOUTH:
                opposite = Direction.NORTH;
                break;
            case WEST:
                opposite = Direction.EAST;
                break;
            case UP:
                opposite = Direction.DOWN;
                break;
            case DOWN:
                opposite = Direction.UP;
                break;
        }
        
        return opposite;
    }
}
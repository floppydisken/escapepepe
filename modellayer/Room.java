package modellayer;

/**
 * Write a description of class Room here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Room
{
    private int floor;
    private int yCoord;
    private int xCoord;
    private boolean visited;
    
    private String description;
    /**
     * Constructor for objects of class Room
     */
    public Room(int floor, int yCoord, int xCoord, String description)
    {
        this.floor = floor;
        this.yCoord = yCoord;
        this.xCoord = xCoord;
        this.description = description;
        visited = false;
    }
    
    public int getFloor()
    {
        return floor;
    }
    
    public int getYCoord()
    {
        return yCoord;
    }
    
    public int getXCoord()
    {
        return xCoord;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public void setVisited()
    {
        visited = true;
    }
    
    public boolean getVisited()
    {
        return visited;
    }
    
    public String toString()
    {
        return "F:" + floor + "-Y:" + yCoord + "-X:" + xCoord;
    }
}
package uilayer;

import uilayer.*;
import controllerlayer.*;
import modellayer.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Thread;

import java.lang.StringBuilder;
/**
 * Write a description of class Game here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Game
{
    // instance variables - replace the example below with your own
    private RoomController roomCtrl;
    private DoorController doorCtrl;
    private ItemController itemCtrl;
    private KeyController keyCtrl;
    private PlayerController playerCtrl;
    private SoundController soundCtrl;
    
    private boolean wonGame = false;
    /**
     * Constructor for objects of class Game
     */
    public Game()
    {
        // initialise instance variables
        roomCtrl = new RoomController();
        doorCtrl = new DoorController();
        itemCtrl = new ItemController();
        keyCtrl = new KeyController();
        playerCtrl = new PlayerController();
        soundCtrl = new SoundController();
        
        createRooms();
        createDoors();
        createItems();
        createKeys();
    }

    public void startGame()
    {
        boolean running = true;
        wonGame = false;
        printWelcome();
        printProgressively("Indtast dit navn");
        playerCtrl.createPlayer(getString());
        playerCtrl.setRoom(roomCtrl.findRoom(0, 2, 2));
        Player player = playerCtrl.getPlayer();
        //printPlayerInfo();
        Command input;
        Room lastRoom = null;
        Key lastKey = null;
        //printHelp();
        
        soundCtrl.startBackgroundMusic();
        
        printProgressively(
            "Spillet går ud på at komme til enden af banen før man har mistet alle sine liv"
           +"\nPas på, PEPE er efter dig! Hver gang du går ind i et rum med 'go' mister du et liv"
           +"\nFor at bevæge dig skal du skrive kommandoen 'go' du får en række mulige retninge "
           +"\ndu kan gå i, såsom 'north', 'east', 'up', 'down'"
           +"\ndet skriver du i sammenhæng med 'go', for eksempel 'go north'."
           +"\nHvis du vil vide hvilke andre muligheder der er, konsulter 'help' kommandoen"
        );
        waitTime(8000);
        
        print("\u000C");
        printProgressively("Du vågner op med et sæt og fornemmer en fugtig, ækel dunst af frø...");
        waitTime(5000);
        
        while(running)
        {
            if (lastRoom != player.getRoom() || lastKey != player.getKey())
            {
                roomCtrl.setVisited(player.getRoom());
                
                if (player.getRoom() instanceof DangerRoom)
                    jumpScare();
                
                print("\u000C"); //Clears screen
                
                printPlayerInfo();
                printMapOfRooms(player);
                
                if (lastRoom != player.getRoom())
                {
                    if (player.getRoom() instanceof DangerRoom)
                    {
                        printProgressively("Du tog " + ((DangerRoom)player.getRoom()).getDamage() + " skade!");
                        soundCtrl.playSound(SoundType.AIRHORN);
                    }
                    
                    if (player.isDead())
                    {
                        soundCtrl.playSound(SoundType.DUNDUN);
                        printProgressively("Pepe fangede dig!");
                        printProgressively("Game over!");
                        printSadPepe();
                        running = false;
                    }
                    else if (wonGame)
                    {
                        printProgressively("Du flygtede fra Pepe!");
                        printSadPepe();
                        running = false;
                    }
                }
            }
                
            if (!player.isDead() && !wonGame)
            {
                println("[help] For mulige kommandoer\n");
                printProgressively(player.getRoom().getDescription());
                lastRoom = player.getRoom();
                lastKey = player.getKey();
                println("");
                print("Mulige retninger:");
                for (Door door : doorCtrl.findDoors(player.getRoom()))
                    print(" " + door.getDirection());
                
                println("");
                
                input = getCommand();
            
                switch (input.getFirstWord())
                {
                   
                    case HELP:
                       printHelp();
                        break;
                    case GO:
                        go(player, input);
                        break;
                    case INSPECT:
                        inspect(player, input);
                        break;
                    case LOOK:
                        lookInRoom(player, input);
                        break;
                    case MAP:
                        printMapOfRooms(player);
                        break;
                    case QUIT:
                        running = false;
                        break;
                }
            }
        }
        
        soundCtrl.stopBackgroundMusic();
    }
    
    private void jumpScare()
    {
        printSadPepe();
        waitTime(100);
    }
    
    private void printSadPepe()
    {
        println("          ████████      ██████");
        println("         █░░░░░░░░██ ██░░░░░░█");
        println("        █░░░░░░░░░░░█░░░░░░░░░█");
        println("       █░░░░░░░███░░░█░░░░░░░░░█");
        println("       █░░░░███░░░███░█░░░████░█");
        println("      █░░░██░░░░░░░░███░██░░░░██");
        println("     █░░░░░░░░░░░░░░░░░█░░░░░░░░███");
        println("    █░░░░░░░░░░░░░██████░░░░░████░░█");
        println("    █░░░░░░░░░█████░░░████░░██░░██░░█");
        println("   ██░░░░░░░███░░░░░░░░░░█░░░░░░░░███");
        println("  █░░░░░░░░░░░░░░█████████░░█████████");
        println("█░░░░░░░░░░█████ ████████   █████    █");
        println("█░░░░░░░░░░█     █ ████    ███ █       █");
        println("█░░░░░░░░░░░░█ ████ ████  ██    ██████");
        println("░░░░░░░░░░░░░█████████░░░████████░░░█");
        println("░░░░░░░░░░░░░░░░█░░░░░█░░░░░░░░░░░░█");
        println("░░░░░░░░░░░░░░░░░░░░██░░░░█░░░░░░██");
        println("░░░░░░░░░░░░░░░░░░██░░░░░░░███████");
        println("░░░░░░░░░░░░░░░░██░░░░░░░░░░█░░░░░█");
        println("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
        println("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
        println("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
        println("░░░░░░░░░░░█████████░░░░░░░░░░░░░░██");
        println("░░░░░░░░░░█▒▒▒▒▒▒▒▒███████████████▒▒█");
        println("░░░░░░░░░█▒▒███████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█");
        println("░░░░░░░░░█▒▒▒▒▒▒▒▒▒█████████████████");
        println("░░░░░░░░░░████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█");
        println("░░░░░░░░░░░░░░░░░░██████████████████");
        println("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█");
        println("██░░░░░░░░░░░░░░░░░░░░░░░░░░░██");
        println("▓██░░░░░░░░░░░░░░░░░░░░░░░░██");
        println("▓▓▓███░░░░░░░░░░░░░░░░░░░░█");
        println("▓▓▓▓▓▓███░░░░░░░░░░░░░░░██");
        println("▓▓▓▓▓▓▓▓▓███████████████▓▓█");
        println("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██");
        println("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█");
        println("▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█");
    }
    
    private void printMapOfRooms(Player player)
    {
        Map m = new Map();
        m.printMap(player);
    }
    
    private void printHelp()
    {
        println("################ Go Command #####################");
        println("# Ved at bruge go command skriv 'go + retning'  #");
        println("# Go command tager imod 4 forskellige retninger #");
        println("# Go West, Go East, Go South, Go North           #");
        println("##################################################");
        println("");
        println("################# Look Command ############################################################################################");
        println("# Ved at bruge look command skal du skrive 'look'                                                                         #");
        println("# Look command bruges til at kigge ind i rummet, for at se hvad der er i rummet                                           #");
        println("# Eksempel: Du er i rum 2, og for at se hvad der er i rummet skal du skrive 'look', ser du en spand, brug inspect command #");
        println("############################################################################################################################");
        println("");
        println("################# Inspect Command ##########################");
        println("# Ved at bruge inspect command skriv 'inspect'             #");
        println("# Inspect command bruges til at undersøge tingene på rumme #");
        println("# Eksempel: du ser en spand, skriv 'inspect spand'          #");
        println("#############################################################");
        println("");
        println("################# Map Command ####################################");
        println("# Ved at bruge map command skal du skriv 'map'                   #");
        println("# Map command bruges til at se hvilken rum man er i              #");
        println("# Eksempel: skriv map for at se hvor alverden befinder du dig hen #");
        println("###################################################################");
        println("");
        println("################# Quit Command #########");
        println("# For at afslutte spillet, skriv 'quit' #");
        println("#########################################");
        println("");
    }
    
    private void printProgressively(String string)
    {
        int stringLength = string.length();
        
        for (int i = 0; i < stringLength; i++)
        {
            print("" + string.charAt(i));
            waitTime(10);
        }
        println("");
    }
    
    private void printPlayerInfo()
    {
        print("Navn: [" + playerCtrl.getPlayer().getName() + "], ");
        print("Liv: [" + playerCtrl.getPlayer().lives() + "], ");
        print("Rum: [" + playerCtrl.getPlayer().getRoom() + "], ");
        print("Nøgler: [" + (playerCtrl.getPlayer().getKey() == null ? "Ingen nøgle" : playerCtrl.getPlayer().getKey()) + "]");
        println("");
    }
    
    private void checkRoomType(Player player)
    {
        Room room = player.getRoom();
        
        if (room instanceof DangerRoom)
        {
            int damage = ((DangerRoom)room).getDamage();
            playerCtrl.takeDamage(damage);
        }
        else if (room instanceof EndRoom)
            wonGame = true;
    }
    
    private void go(Player player, Command input)
    {
        Direction direction = input.getDirection();
        if(direction == null)
            printWrongDirection();
        else
        {
            Door door = doorCtrl.findDoor(player.getRoom(), direction);
            if(door == null)
                printWrongDirection();
            else
            {
                Key key = keyCtrl.findKey(door);
                
                if(key == null)
                {
                    playerCtrl.setRoom(roomCtrl.getNextRoom(player.getRoom(), direction));
                    checkRoomType(player);
                }
                else if (key == player.getKey())
                {
                    soundCtrl.playSound(SoundType.DOOROPEN);
                    playerCtrl.setRoom(roomCtrl.getNextRoom(player.getRoom(), direction));
                    playerCtrl.setKey(null);
                    keyCtrl.removeFromDoor(key);
                    checkRoomType(player);
                }
                else
                    printProgressively("Døren er låst og du har ikke den korrekte nøgle");
            }
        }
    }
    
    private void inspect(Player player, Command input)
    {
        if(input.getSecondWord() == null)
            printProgressively("Hvad vil du gerne kigge i?");
        else
        {
            Item item = itemCtrl.findItem(player.getRoom(), input.getSecondWord());
            
            if(item == null)
                printProgressively("Det findes ikke i rummet");
            else 
            {
                Key key = keyCtrl.findKey(item);
                
                soundCtrl.playSound(SoundType.FOUNDITEM);
                
                if(key == null)
                    printProgressively("Du finder intet");
                else
                {
                    printProgressively("Du har fundet: " + key.getDescription());
                    playerCtrl.setKey(key);
                    keyCtrl.removeFromItem(key);
                    waitTime(2000);
                }
            }
        }
    }
    
    private void lookInRoom(Player player, Command input)
    {
        ArrayList<Item> items = itemCtrl.findItems(player.getRoom());
        StringBuilder itemsString = new StringBuilder();
        
        for (Item item : items)
            itemsString.append(item.getName() + ", ");
        
        if (itemsString.length() > 0)
            itemsString.setLength(itemsString.length() - 2);

        printProgressively("Du ser dig omkring og ser der er følgende ting i rummet: " + itemsString.toString());
    }
    
    private String getString()
    {
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextLine();
    }
    
    private Command getCommand()
    {
        return Command.parseCommand(getString());
    }
    
    private void printWrongDirection()
    {
        printProgressively("Du slog næsen, da du løb ind i væggen!");
    }
    
    private void printWelcome()
    {
        printProgressively("Velkommen til EscapePepe");
    }
    
    private void println(String line)
    {
        System.out.println(line);
    }
    
    private void print(String text)
    {
        System.out.print(text);
    }
    
    private void waitTime() { waitTime(2000); }
    private void waitTime(long time)
    {
        try
        {
            Thread.sleep(time); //Sleeps thread in 5000 miliseconds.
        }
        catch (InterruptedException ie) //Catches if it gets interrupted.
        {
            
        }
    }
    
    public static boolean checkDoorIntegrety()
    {
        boolean result = true;
        
        Game g = new Game();
        
        for (Door d : g.doorCtrl.getAllDoors())
        {
            Direction dir = d.getDirection();
            Door oppositeDoor = g.doorCtrl.findDoor(g.roomCtrl.getNextRoom(d.getRoomDoorIsIn(), dir), dir.getOpposite());
            
            if (oppositeDoor == null || g.roomCtrl.getNextRoom(oppositeDoor.getRoomDoorIsIn(), dir.getOpposite()) != d.getRoomDoorIsIn())
            {
                result = false;
                System.out.println("Door in room " + d.getRoomDoorIsIn() + " going " + d.getDirection() + " is missing opposite door!");
            }
        }
        
        return result;
    }
    
    private void createRooms()
    {
        roomCtrl.createRoom(0, 2, 2, "Der er mørk og koldt i rummet.");
        roomCtrl.createRoom(0, 2, 1, "Du lugter og der er muggent. Du ser en spand på gulvet.");
        roomCtrl.createRoom(0, 2, 3, "Du ser en luge over dig");
        roomCtrl.createDangerRoom(0, 3, 2, "Du træder i en bjørnefælde. Den kapper dit ben.", 5);
        roomCtrl.createRoom(1, 1, 2, "Du befinder dig i et køkken, du ser nogle mærkværdige ting omkring dig");
        roomCtrl.createRoom(1, 1, 3, "SM rummet er rødt og fyldt med lugt af sved");
        roomCtrl.createRoom(1, 1, 4, "Det hele ryster, du står i et tomt værelse");
        roomCtrl.createRoom(1, 2, 2, "Værelset er fyldt med blod, du tisser lidt i bukserne");
        roomCtrl.createRoom(1, 2, 3, "Første stue rum"); 
        roomCtrl.createRoom(1, 2, 4, "Dette er mystisk, du forstår ikke hvor du er");
        roomCtrl.createRoom(1, 3, 2, "Du står nu i garagen, du kan se en masse mærkelige ting omkring dig");
        roomCtrl.createDangerRoom(1, 3, 3, "Du trådte i SM fælden og bliver nu straffet", 5);
        roomCtrl.createRoom(1, 3, 4, "Du er nu i gildesalen, der er tomt og højlydt");
        roomCtrl.createRoom(1, 4, 2, "Du står nu i bryggerset, det dufter dajligt");
        roomCtrl.createRoom(1, 4, 3, "Du står i et rum fyldt med Pepe bamser, du begynder at græde");
        roomCtrl.createRoom(1, 4, 4, "Du befinder dig nu i et rum der ligner et skab");
        roomCtrl.createRoom(1, 5, 2, "Der dufter lidt af prut, måske har Pepe lige været her");
        roomCtrl.createEndRoom(1, 5, 3, "WIN ROOM\nDu flygtede fra Pepe og hans SM");
        roomCtrl.createRoom(1, 5, 4, "Du står i en vandpyt, du ligger mærke til den grimme lampe");
    }
    
    private void createDoors()
    {
        doorCtrl.createDoor(0, 2, 2, Direction.WEST);
        doorCtrl.createDoor(0, 2, 2, Direction.EAST);
        doorCtrl.createDoor(0, 2, 2, Direction.SOUTH);
        
        doorCtrl.createDoor(0, 2, 1, Direction.EAST);
        
        doorCtrl.createDoor(0, 2, 3, Direction.WEST);
        doorCtrl.createDoor(0, 2, 3, Direction.UP);
        
        doorCtrl.createDoor(0, 3, 2, Direction.NORTH);
        
        doorCtrl.createDoor(1, 1, 2, Direction.SOUTH);
        
        doorCtrl.createDoor(1, 1, 3, Direction.EAST);
        doorCtrl.createDoor(1, 1, 3, Direction.SOUTH);

        doorCtrl.createDoor(1, 1, 4, Direction.WEST);
        doorCtrl.createDoor(1, 1, 4, Direction.SOUTH);

        doorCtrl.createDoor(1, 2, 2, Direction.NORTH);
        doorCtrl.createDoor(1, 2, 2, Direction.EAST);

        doorCtrl.createDoor(1, 2, 3, Direction.WEST);
        doorCtrl.createDoor(1, 2, 3, Direction.NORTH);
        doorCtrl.createDoor(1, 2, 3, Direction.EAST);
        doorCtrl.createDoor(1, 2, 3, Direction.DOWN);

        doorCtrl.createDoor(1, 2, 4, Direction.WEST);
        doorCtrl.createDoor(1, 2, 4, Direction.NORTH);
        doorCtrl.createDoor(1, 2, 4, Direction.SOUTH);
        
        doorCtrl.createDoor(1, 3, 2, Direction.EAST);
        doorCtrl.createDoor(1, 3, 2, Direction.SOUTH);

        doorCtrl.createDoor(1, 3, 3, Direction.WEST);
        doorCtrl.createDoor(1, 3, 3, Direction.EAST);

        doorCtrl.createDoor(1, 3, 4, Direction.WEST);
        doorCtrl.createDoor(1, 3, 4, Direction.NORTH);
        doorCtrl.createDoor(1, 3, 4, Direction.SOUTH);

        doorCtrl.createDoor(1, 4, 2, Direction.NORTH);
        doorCtrl.createDoor(1, 4, 2, Direction.EAST);

        doorCtrl.createDoor(1, 4, 3, Direction.WEST);
        doorCtrl.createDoor(1, 4, 3, Direction.EAST);
        doorCtrl.createDoor(1, 4, 3, Direction.SOUTH);

        doorCtrl.createDoor(1, 4, 4, Direction.WEST);
        doorCtrl.createDoor(1, 4, 4, Direction.NORTH);
        doorCtrl.createDoor(1, 4, 4, Direction.SOUTH);
        
        doorCtrl.createDoor(1, 5, 2, Direction.EAST);
        
        doorCtrl.createDoor(1, 5, 3, Direction.WEST);
        doorCtrl.createDoor(1, 5, 3, Direction.NORTH);

        doorCtrl.createDoor(1, 5, 4, Direction.NORTH);
    }
    
    private void createItems()
    {
        itemCtrl.createItem("Spand", roomCtrl.findRoom(0, 2, 1));
        itemCtrl.createItem("Endeløs brønd", roomCtrl.findRoom(1, 1, 2));
        itemCtrl.createItem("Kinesisk krukke", roomCtrl.findRoom(1, 1, 2));
        itemCtrl.createItem("Russisk dukke", roomCtrl.findRoom(1, 3, 2));
        itemCtrl.createItem("Smadret cykel", roomCtrl.findRoom(1, 3, 2));
        itemCtrl.createItem("Lille grim lampe", roomCtrl.findRoom(1, 5, 4));
        itemCtrl.createItem("Barnevogn uden hjul", roomCtrl.findRoom(1, 5, 4));
        itemCtrl.createItem("Uåbnet fødselsdagsgave", roomCtrl.findRoom(1, 5, 4));
    }
    
    private void createKeys()
    {
        keyCtrl.createKey("Rød nøgle", itemCtrl.findItem(0, 2, 1, "Spand"), doorCtrl.findDoor(0, 2, 3, Direction.UP));
        keyCtrl.createKey("Rusten koben", itemCtrl.findItem(1, 1, 2, "Kinesisk krukke"), doorCtrl.findDoor(1, 2, 4, Direction.SOUTH));
        keyCtrl.createKey("Motorsav", itemCtrl.findItem(1, 3, 2, "Russisk dukke"), doorCtrl.findDoor(1, 4, 4, Direction.SOUTH));
        keyCtrl.createKey("Økse", itemCtrl.findItem(1, 5, 4, "Uåbnet fødselsdagsgave"), doorCtrl.findDoor(1, 5, 3, Direction.SOUTH));
    }
}

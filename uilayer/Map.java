package uilayer;
import controllerlayer.*;
import modellayer.*;

import java.util.ArrayList;
import java.lang.StringBuilder;

/**
 * Write a description of class Map here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Map
{
    RoomController roomCtrl;
    DoorController doorCtrl;
    
    /**
     * Constructor for objects of class Map
     */
    public Map()
    {
        roomCtrl = new RoomController();
        doorCtrl = new DoorController();
    }
    
    public void printMap(Player player)
    {
        ArrayList<StringBuilder> lines = new ArrayList<>();
        
        Room playerRoom = player.getRoom();
        int lowestYCoord = roomCtrl.getLowestYCoord(playerRoom.getFloor());
        int lowestXCoord = roomCtrl.getLowestXCoord(playerRoom.getFloor());
        int highestYCoord = roomCtrl.getHighestYCoord(playerRoom.getFloor());
        int highestXCoord = roomCtrl.getHighestXCoord(playerRoom.getFloor());
        
        for (int y = lowestYCoord; y <= highestYCoord; y++)
        {
            StringBuilder line1 = new StringBuilder();
            StringBuilder line2 = new StringBuilder();
            StringBuilder line3 = new StringBuilder();
            StringBuilder line4 = new StringBuilder();
            StringBuilder line5 = new StringBuilder();
            lines.add(line1);
            lines.add(line2);
            lines.add(line3);
            lines.add(line4);
            lines.add(line5);
            
            for (int x = lowestXCoord; x <= highestXCoord; x++)
            {
                Room room = roomCtrl.findRoom(playerRoom.getFloor(), y, x);
                if (room == null || !room.getVisited())
                {
                    line1.append("░░░░░");//┌┘░└┐
                    line2.append("░░░░░");//┘░░░│
                    line3.append("░░░░░");//░░░░│
                    line4.append("░░░░░");//┐░░░│
                    line5.append("░░░░░");//└───┘
                }
                else
                {
                    char filler = (room instanceof DangerRoom) ? '▓' : '░';
                    
                    if (doorCtrl.findDoor(room, Direction.NORTH) != null)
                        line1.append("┌┘" + filler + "└┐");
                    else
                        line1.append("┌───┐");
                    
                    if (doorCtrl.findDoor(room, Direction.WEST) != null)
                    {
                        line2.append("┘" + filler + filler);
                        line3.append("" + filler + filler); //╳ or nothing gets inserted here depending on player location
                        line4.append("┐" + filler + filler);
                    }
                    else
                    {
                        line2.append("│" + filler + filler);
                        line3.append("│" + filler); //╳ or nothing gets inserted here depending on player location
                        line4.append("│" + filler + filler);
                    }
                    
                    if (room == playerRoom)
                        line3.append("╳"); //╳
                    else
                        line3.append("" + filler);
                    
                    if (doorCtrl.findDoor(room, Direction.EAST) != null)
                    {
                        line2.append("" + filler + "└");
                        line3.append("" + filler + filler);
                        line4.append("" + filler + "┌");
                    }
                    else
                    {
                        line2.append("" + filler + "│");
                        line3.append("" + filler + "│");
                        line4.append("" + filler + "│");
                    }
                    
                    if (doorCtrl.findDoor(room, Direction.SOUTH) != null)
                        line5.append("└┐" + filler + "┌┘");
                    else
                        line5.append("└───┘");
                }
            }
        }
            
        for (StringBuilder line : lines)
            System.out.println(line.toString());
    }
}
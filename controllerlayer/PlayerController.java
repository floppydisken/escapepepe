package controllerlayer;
import modellayer.*;

/**
 * Write a description of class PlayerController here.
 *
 * @author (Stefan)
 * @version (a version number or a date)
 */
public class PlayerController
{
    // instance variables - replace the example below with your own
    private Player player;

    /**
     * Constructor for objects of class PlayerController
     */
    public PlayerController()
    {
     //this.player = player;
    }
    
    public void createPlayer(String name)
    {
        player = new Player(name);
    }
    
    public Player getPlayer()
    {
        return player;
    }
    
    public void setRoom(Room room)
    {
        if(playerExists())
        {
            player.setRoom(room);
            player.takeDamage();
        }           
    }
    
    public void setKey(Key key)
    {
        if(playerExists())
            player.setKey(key);
        
    }
    
    public void takeDamage(int damage)
    {
        if(playerExists())
            player.takeDamage(damage);
    }
    
    private boolean playerExists()
    {
        if(player == null)
        {
            System.out.println("Player is not created, please do so!");
            return false;
        }
        else
        {
            return true;
        }
    }
    
}

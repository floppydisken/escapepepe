package controllerlayer;
import modellayer.*;
import java.util.ArrayList;
/**
 * Write a description of class DoorController here.
 *
 * @author Stiig
 * @version 04/04
 */
public class DoorController
{
    DoorContainer doors;
    
    /**
     * Constructor for objects of class DoorController
     */
    public DoorController()
    {
        doors = DoorContainer.getInstance();
    }
    
    public Door createDoor(int floor, int yCoord, int xCoord, Direction direction)
    {
        RoomController roomCtrl = new RoomController();
        return createDoor(roomCtrl.findRoom(floor, yCoord, xCoord), direction);
    }
    
    /**
     * Creates a door and returns it.
     */
    public Door createDoor(Room inRoom, Direction direction)
    {
        Door door = new Door(inRoom, direction);
        doors.addDoor(door);
        return door;
    }
    
    public Door findDoor(int floor, int roomYCoord, int roomXCoord, Direction direction)
    {
        RoomController roomCtrl = new RoomController();
        return doors.findDoor(roomCtrl.findRoom(floor, roomYCoord, roomXCoord), direction);
    }
    
    /**
     * Finds a door from parameters. Returns null if none is found.
     */
    public Door findDoor(Room room, Direction direction)
    {
        return doors.findDoor(room, direction);
    }
    
    public ArrayList<Door> findDoors(Room room)
    {
        return doors.findDoors(room);
    }
    
    public ArrayList<Door> getAllDoors()
    {
        return doors.getAllDoors();
    }
}
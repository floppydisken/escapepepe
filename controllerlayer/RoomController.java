package controllerlayer;
import modellayer.*;

import java.util.ArrayList;
/**
 * Write a description of class RoomController here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RoomController
{
    // instance variables - replace the example below with your own
    RoomContainer roomContainer;
    /**
     * Constructor for objects of class RoomController
     */
    public RoomController()
    {
        // initialise instance variables
      roomContainer = RoomContainer.getInstance();
    }
    
    public Room createRoom(int floor, int yCoord, int xCoord, String description)
    {
        Room room = new Room(floor, yCoord, xCoord, description);
        roomContainer.addRoom(room);
        return room;
    }
    
    public DangerRoom createDangerRoom(int floor, int yCoord, int xCoord, String description, int damage)
    {
        DangerRoom dangerRoom = new DangerRoom(floor, yCoord, xCoord, description, damage);
        roomContainer.addRoom(dangerRoom);
        return dangerRoom;
    }
    
    public EndRoom createEndRoom(int floor, int yCoord, int xCoord, String description)
    {
        EndRoom endRoom = new EndRoom(floor, yCoord, xCoord, description);
        roomContainer.addRoom(endRoom);
        return endRoom;
    }
    
    public void setVisited(Room room)
    {
        room.setVisited();
    }
    
    public Room findRoom(int floor, int yCoord, int xCoord)
    {
        return roomContainer.findRoom(floor, yCoord, xCoord);
    }
    
    public ArrayList<Room> findRooms()
    {
        return roomContainer.findRooms();
    }
    
    public Room getNextRoom(Room room, Direction direction)
    {
        int floor = room.getFloor();
        int yCoord = room.getYCoord();
        int xCoord = room.getXCoord();
        
        switch (direction)
        {
            case NORTH:
                yCoord--;
                break;
            case EAST:
                xCoord++;
                break;
            case SOUTH:
                yCoord++;
                break;
            case WEST:
                xCoord--;
                break;
            case UP:
                floor++;
                break;
            case DOWN:
                floor--;
                break;
        }
        
        return roomContainer.findRoom(floor, yCoord, xCoord);
    }
    
    public int getLowestYCoord(int floor)
    {
        return roomContainer.getLowestYCoord(floor);
    }
    
    public int getLowestXCoord(int floor)
    {
        return roomContainer.getLowestXCoord(floor);
    }
    
    public int getHighestYCoord(int floor)
    {
        return roomContainer.getHighestYCoord(floor);
    }
    
    public int getHighestXCoord(int floor)
    {
        return roomContainer.getHighestXCoord(floor);
    }
}

package controllerlayer;

import modellayer.*;

import java.util.Random;

/**
 * Write a description of class SoundController here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SoundController
{
    private BackgroundPlayer backgroundMusic;
    
    /**
     * Constructor for objects of class SoundController
     */
    public SoundController()
    {
        // Continuosly plays throughout.
        backgroundMusic = new BackgroundPlayer("/sounds/RunescapeAttentionTrack.mp3");
    }
    
    public void startBackgroundMusic()
    {
        backgroundMusic.start();
    }
    
    public void stopBackgroundMusic()
    {
        backgroundMusic.stop();
    }
    
    public void playSound(SoundType type)
    {
        String soundPath = "";
        
        switch (type)
        {
            case AIRHORN:
                soundPath = "/sounds/airhornshort.mp3";
                break;
            case DOOROPEN:
                soundPath = "/sounds/DoorOpen.mp3";
                break;
            case DUNDUN:
                soundPath = "/sounds/sound1.mp3";
                break;
            case FOUNDITEM:
                String[] inspectSoundFiles = 
                { 
                    "/sounds/InspectItem.mp3", 
                    "/sounds/InspectItem2.mp3", 
                    "/sounds/InspectItem3.mp3",
                    "/sounds/FoundItem.mp3",
                    "/sounds/FoundItem2.mp3"
                };
                
                soundPath = inspectSoundFiles[new Random().nextInt(inspectSoundFiles.length)];
                break;
        }
        
        new SoundPlayer(soundPath).start();
    }
}
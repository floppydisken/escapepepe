package controllerlayer;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
//import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.Player;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.decoder.JavaLayerException;

/**
 * Write a description of class SoundPlayer here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SoundPlayer extends Thread
{ 
    private String audioFile;
    private FileInputStream fileStream;
    
    private Player player;
    
    public SoundPlayer(String audioFile) 
    {
         this.audioFile = audioFile;
         
         // Fetch resource InputStream directly
         player = setupPlayer(getInputStream());
         
    }
    
    protected InputStream getInputStream()
    {
        return getClass().getResourceAsStream(audioFile);
    }

    protected Player setupPlayer(InputStream stream)
    {
        Player tempPlayer = null;
        try
        {
            tempPlayer = new Player(stream);
        }
        catch (JavaLayerException e)
        {
            System.out.println("Player failed to load InputStream");
            System.out.println(e);
        }
        
        return tempPlayer;
    }
    
    public void run()
    {
        try
        {
            player.play();
        }
        catch (JavaLayerException e)
        {
            System.out.println("Player failed to play InputStream");
            System.out.println(e);
        }
        catch (Exception e)
        {
            return;
        }
    }
    
    public Player getPlayer()
    {
        return player;
    }
}

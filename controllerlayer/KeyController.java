package controllerlayer;
import modellayer.*;

import java.util.ArrayList;
import java.util.Random;

/**
 * Write a description of class KeyController here.
 *
 * @author Stiig
 * @version 04/04
 */
public class KeyController
{
    private KeyContainer keyContainer;
    
    /**
     * Constructor for objects of class KeyController
     */
    public KeyController()
    {
        keyContainer = KeyContainer.getInstance();
    }
    
    public Key createKey(String description, Room room, Door door)
    {
        ItemController itemCtrl = new ItemController();
        ArrayList<Item> items = itemCtrl.findItems(room);
        
        Item itemToPutIn = items.get(new Random().nextInt(items.size()));
        
        return createKey(description, itemToPutIn, door);
    }
    
    public Key createKey(String description, Item item, Door door)
    {
        Key key = new Key(description, item, door);
        keyContainer.addKey(key);
        
        return key;
    }
    
    public void removeFromItem(Key key)
    {
        key.removeFromItem();
    }
    
    public void removeFromDoor(Key key)
    {
        key.removeFromDoor();
    }
    
    public Key findKey(Item item)
    {
        return keyContainer.findKey(item);
    }
    
    public Key findKey(Door door)
    {
        return keyContainer.findKey(door);
    }
}
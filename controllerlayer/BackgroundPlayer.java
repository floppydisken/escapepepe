package controllerlayer;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 * Write a description of class BackGroundPlayer here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class BackgroundPlayer extends SoundPlayer
{
    
    public BackgroundPlayer(String audioFile)
    {
        super(audioFile);
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            Player player = setupPlayer(getInputStream());
            try
            {
                player.play();
            }
            catch (JavaLayerException e)
            {
                System.out.println("Background music failed");
            }

        }
    }
}
